import {ADD_PATIENT, SET_PATIENTS} from './types';

const reducer = (state = {list: [], byId: {}}, {type, payload}) => {
  switch (type) {
    case SET_PATIENTS:
      const newList = payload.map(({id}) => id).filter(id => !state.list.includes(id));
      const newById = payload.reduce((acc, patient) => Object.assign(acc, {[patient.id]: patient}), {});

      return {
        list: payload.map(({id}) => id),
        byId: payload.reduce((acc, patient) => Object.assign(acc, {[patient.id]: patient}), {}),
      };
    case ADD_PATIENT:
      return {
        list: [...state.list, payload.id],
        byId: {...state.byId, [payload.id]: payload}
      };
    default:
      return state;
  }
};

export default reducer;

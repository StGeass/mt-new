import uniqid from 'uniqid';
import {push} from 'react-router-redux';

import * as patientActions from "./actions";

export const getPatients = () => (dispatch, getState) => {
  console.log('getPatients');
  return $p.adapters.pouch.remote.ram.allDocs()
    .then(({rows}) => {
      const docs = [];

      rows
        .filter(({id}) => /^cat\.Patients/.test(id))
        .forEach(({id}) => docs.push($p.adapters.pouch.remote.ram.get(id)));

      return Promise.all(docs);
    })
    .then(patients => {
      dispatch(patientActions.setPatients(patients));
    })
};

export const addPatient = (data) => (dispatch, getState) => {
  return Promise.resolve()
    .then(() => ({id: uniqid(), ...data}))
    .then(({id, name, password, email}) => {
      const payload = {
        "_id": `cat.Patients|${id}`,
        "predefined_name": name,
        id,
        name,
        password,
        email,
      };

      dispatch(patientActions.addPatient(payload));

      $p.adapters.pouch.local.ram.put(payload).catch(console.error);
      $p.adapters.pouch.remote.ram.put(payload).catch(console.error);
    })
    .then(() => {
      dispatch(push('/patients/list'));
    });
};

import {ADD_PATIENT, SET_PATIENTS} from "./types";

export const addPatient = (payload) => ({
  type: ADD_PATIENT,
  payload,
});

export const setPatients = (payload) => ({
  type: SET_PATIENTS,
  payload,
});

import React, {PureComponent} from 'react';

import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Helmet from 'react-helmet';
import List from './List';
import compose from 'recompose/compose';
import withStyles from 'metadata-react/styles/paper600';
import {withIface, withPrm} from 'metadata-redux';
import {connect} from "react-redux";
import {getPatients} from "../../redux/patients/operations";

const ltitle = 'Список пациентов';

class PatientList extends PureComponent {

  render() {
    const {classes, title, patients} = this.props;

    if (title != ltitle) {
      this.props.handleIfaceState({
        component: '',
        name: 'title',
        value: ltitle,
      });
    }

    return (
      <Paper className={classes.root} elevation={4}>
        <Helmet title={ltitle}/>

        <Typography variant="title" style={{paddingTop: 16}}>
          {ltitle}
        </Typography>

        <div style={{padding: '20px 0 30px'}}>
          <List {...this.props} />
        </div>
      </Paper>
    );
  }

}

const connected = connect(
  ({patients}) => ({
    patients: patients.list.map((id) => patients.byId[id]),
  }),
  {
    getPatients,
  }
)(PatientList);


export default compose(withStyles, withIface, withPrm)(connected);

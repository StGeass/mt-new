import React, {PureComponent, Fragment} from 'react';
import PropTypes from 'prop-types';

import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import DialogActions from '@material-ui/core/DialogActions';
import Helmet from 'react-helmet';
import Typography from '@material-ui/core/Typography';
import IconError from '@material-ui/icons/ErrorOutline';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormGroup from '@material-ui/core/FormGroup';
import FormControl from '@material-ui/core/FormControl';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import CircularProgress from '@material-ui/core/CircularProgress';
import classnames from 'classnames';

import {withPrm} from 'metadata-redux';
import withStyles from '../../../md-components/styles/paper600';


class AddPatientForm extends PureComponent {

  constructor(props) {
    super(props);

    this.state = {
      index: 0,
      email: '',
      name: '',
      password: '',
      showPassword: false,
    };

    this.handleAdd = this.handleAdd.bind(this);
  }

  componentDidMount() {
    const {getPatients} = this.props;

    getPatients();
  }

  handleAdd() {
    const {props: {addPatient}, state: {name, email, password}} = this;

    if (name && email && password && addPatient) {
      addPatient({name, password, email});
    }
  }

  handleMouseDownPassword(event) {
    event.preventDefault();
  }

  handleClickShowPasssword = () => {
    this.setState({showPassword: !this.state.showPassword});
  };

  render() {

    const {props, state, handleAdd} = this;
    const {patients} = props;

    return <Fragment>
      {
        patients && patients.length ? (
          patients.map((patient, key) => (
            <div key={patient.id} style={{
              margin: '5px', display: 'flex', width: '100%',
              justifyContent: 'space-between'
            }}>
              <div>
                #{key + 1}
              </div>

              <div style={{marginLeft: '16px', flexGrow: 1}}>
                {patient.name}
              </div>

              <div style={{marginLeft: '16px'}}>
                {patient.email}
              </div>
            </div>
          ))
        ) : 'Список пуст'
      }

    </Fragment>;
  }
}

export default withPrm(withStyles(AddPatientForm));

import React from 'react';

import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Helmet from 'react-helmet';
import AddPatientForm from './AddPatientForm';
import compose from 'recompose/compose';
import withStyles from 'metadata-react/styles/paper600';
import {withIface, withPrm} from 'metadata-redux';
import {connect} from "react-redux";
import {addPatient} from "../../redux/patients/operations";

const ltitle = 'Регистрация нового пациента';

function AddPatient(props) {

  const {classes, title} = props;

  if (title != ltitle) {
    props.handleIfaceState({
      component: '',
      name: 'title',
      value: ltitle,
    });
  }

  return (
    <Paper className={classes.root} elevation={4}>
      <Helmet title={ltitle}/>

      <Typography variant="title" style={{paddingTop: 16}}>
        Введите данные пациента
      </Typography>

      <AddPatientForm {...props} />
    </Paper>
  );

}

const connected = connect((state) => ({}), {
  addPatient,
})(AddPatient);

export default compose(withStyles, withIface, withPrm)(connected);

import React, {Component, Fragment} from 'react';

import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import DialogActions from '@material-ui/core/DialogActions';
import Helmet from 'react-helmet';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormGroup from '@material-ui/core/FormGroup';
import FormControl from '@material-ui/core/FormControl';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';

import {withPrm} from 'metadata-redux';
import withStyles from '../../../md-components/styles/paper600';


class AddPatientForm extends Component {

  constructor(props) {
    super(props);

    this.state = {
      index: 0,
      email: '',
      name: '',
      password: '',
      showPassword: false,
    };

    this.handleAdd = this.handleAdd.bind(this);
  }

  handleAdd() {
    const {props: {addPatient}, state: {name, email, password}} = this;
  console.log({name, email, password, addPatient});
    if (name && email && password && addPatient) {
      addPatient({name, password, email});
    }
  }

  handleMouseDownPassword(event) {
    event.preventDefault();
  }

  handleClickShowPasssword = () => {
    this.setState({showPassword: !this.state.showPassword});
  };

  render() {

    const {props, state} = this;
    const {classes} = props;

    return <Fragment>

      <Helmet title={props.title}>
        <meta name="description" content="Вход в систему"/>
      </Helmet>

      <FormGroup>

        <TextField
          label="ФИО"
          InputProps={{placeholder: 'ФИО'}}
          fullWidth
          margin="dense"
          value={state.name}
          onChange={event => this.setState({name: event.target.value})}
        />

        <TextField
          label="Почта"
          InputProps={{placeholder: 'Почта'}}
          fullWidth
          margin="dense"
          value={state.email}
          onChange={event => this.setState({email: event.target.value})}
        />

        <FormControl
          fullWidth
          margin="dense"
        >
          <InputLabel>Пароль</InputLabel>
          <Input
            type={state.showPassword ? 'text' : 'password'}
            placeholder="password"
            value={state.password}
            onChange={event => this.setState({password: event.target.value})}
            onKeyPress={(e) => e.key === 'Enter' && this.handleAdd()}
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  onClick={this.handleClickShowPasssword}
                  onMouseDown={this.handleMouseDownPassword}
                >
                  {this.state.showPassword ? <VisibilityOff/> : <Visibility/>}
                </IconButton>
              </InputAdornment>
            }
          />
        </FormControl>

      </FormGroup>

      <DialogActions>
        <Button size="small" className={classes.button} onClick={this.handleAdd}>Сохранить</Button>
      </DialogActions>

    </Fragment>;
  }
}

export default withPrm(withStyles(AddPatientForm));

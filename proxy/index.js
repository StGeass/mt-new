const cors = require('cors');
const express = require('express');
const bodyParser = require('body-parser');

const routes = require('./routes'); //importing route

const app = express();
const port = process.env.PORT || 3001;

app.use(cors());


// app.use('/couchdb/hw_', apiRoutes);
// app.use('/', localhostRoutes);

routes(app); //register the route

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.listen(port);

console.log('Proxy server started on: ' + port);

const url = require('url');
const http = require('http');
const https = require('https');
const cors = require('cors');
const requestProxy = require('express-request-proxy');

// const API_PATH = 'http://94.130.182.118:5984/_utils/fauxton';
const API_PATH = 'http://195.201.96.73/couchdb/hw_';
const LOCALHOST_PATH = 'http://localhost:3000';
const API_REQUEST = '/couchdb/hw_*';
const LOCALHOST_REQUEST = '/*';

module.exports = app => {
  app.route(API_REQUEST)
    .options(cors())
    .get(httpsAnyGet)
    .all(requestProxy({
      url: API_PATH + API_REQUEST,
    }));

  app.route(LOCALHOST_REQUEST)
    .options(cors())
    .get(httpAnyGet)
    .all(requestProxy({
      url: API_PATH + LOCALHOST_REQUEST,
    }));
};

function httpAnyGet(req, res) {
  const URL_PATH = url.parse(req.url, true).path;

  http.get(LOCALHOST_PATH + URL_PATH, (resp) => {
    let data = '';

    // A chunk of data has been recieved.
    resp.on('data', (chunk) => {
      data += chunk;
    });

    // The whole response has been received. Print out the result.
    resp.on('end', () => {
      res.set('Content-Type', 'text/html');
      res.send(new Buffer(data));
    });

  }).on("error", (err) => {
    res.send(err);
    console.log("Error: " + err.message);
  });
}

function httpsAnyGet(req, res) {
  const URL_PATH = url.parse(req.url, true).path;

  https.get(API_PATH + URL_PATH, (resp) => {
    let data = '';

    // A chunk of data has been recieved.
    resp.on('data', (chunk) => {
      data += chunk;
    });

    // The whole response has been received. Print out the result.
    resp.on('end', () => {
      try {
        res.json(JSON.parse(data));
      } catch (e) {
        console.error('Error:', e);
        console.error('Data:', data);
      }
    });

  }).on("error", (err) => {
    res.send(err);
    console.log("Error: " + err.message);
  });
}